/********************************************************************************
** Form generated from reading UI file 'Task.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASK_H
#define UI_TASK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Task
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_7;
    QSpacerItem *verticalSpacer;
    QLabel *assignee;
    QCheckBox *checkbox;
    QLabel *descripd;
    QLabel *deadlined;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *removeButton;
    QGridLayout *gridLayout_2;
    QPushButton *descripbutton;
    QPushButton *assignbutton;
    QPushButton *editButton;
    QPushButton *deadlinebutton;

    void setupUi(QWidget *Task)
    {
        if (Task->objectName().isEmpty())
            Task->setObjectName(QString::fromUtf8("Task"));
        Task->resize(911, 535);
        horizontalLayout = new QHBoxLayout(Task);
        horizontalLayout->setSpacing(4);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(-1, -1, 17, -1);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        assignee = new QLabel(Task);
        assignee->setObjectName(QString::fromUtf8("assignee"));

        verticalLayout_7->addWidget(assignee, 0, Qt::AlignHCenter);

        checkbox = new QCheckBox(Task);
        checkbox->setObjectName(QString::fromUtf8("checkbox"));

        verticalLayout_7->addWidget(checkbox, 0, Qt::AlignHCenter|Qt::AlignVCenter);

        descripd = new QLabel(Task);
        descripd->setObjectName(QString::fromUtf8("descripd"));

        verticalLayout_7->addWidget(descripd, 0, Qt::AlignHCenter|Qt::AlignVCenter);

        deadlined = new QLabel(Task);
        deadlined->setObjectName(QString::fromUtf8("deadlined"));

        verticalLayout_7->addWidget(deadlined, 0, Qt::AlignHCenter);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_7);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        removeButton = new QPushButton(Task);
        removeButton->setObjectName(QString::fromUtf8("removeButton"));

        horizontalLayout->addWidget(removeButton);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(6, -1, -1, -1);
        descripbutton = new QPushButton(Task);
        descripbutton->setObjectName(QString::fromUtf8("descripbutton"));

        gridLayout_2->addWidget(descripbutton, 6, 0, 1, 1);

        assignbutton = new QPushButton(Task);
        assignbutton->setObjectName(QString::fromUtf8("assignbutton"));

        gridLayout_2->addWidget(assignbutton, 5, 0, 1, 1);

        editButton = new QPushButton(Task);
        editButton->setObjectName(QString::fromUtf8("editButton"));

        gridLayout_2->addWidget(editButton, 0, 0, 1, 1, Qt::AlignHCenter|Qt::AlignVCenter);

        deadlinebutton = new QPushButton(Task);
        deadlinebutton->setObjectName(QString::fromUtf8("deadlinebutton"));

        gridLayout_2->addWidget(deadlinebutton, 1, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);


        retranslateUi(Task);

        QMetaObject::connectSlotsByName(Task);
    } // setupUi

    void retranslateUi(QWidget *Task)
    {
        Task->setWindowTitle(QCoreApplication::translate("Task", "Form", nullptr));
        assignee->setText(QCoreApplication::translate("Task", "Assigned to:", nullptr));
        checkbox->setText(QCoreApplication::translate("Task", "Buy Milk", nullptr));
        descripd->setText(QCoreApplication::translate("Task", "Description:", nullptr));
        deadlined->setText(QCoreApplication::translate("Task", "Deadline:", nullptr));
        removeButton->setText(QCoreApplication::translate("Task", "Remove", nullptr));
        descripbutton->setText(QCoreApplication::translate("Task", "Add description", nullptr));
        assignbutton->setText(QCoreApplication::translate("Task", "Assign task", nullptr));
        editButton->setText(QCoreApplication::translate("Task", "Edit", nullptr));
        deadlinebutton->setText(QCoreApplication::translate("Task", "Set deadline", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Task: public Ui_Task {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASK_H
