#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>
#include <QDate>

Task::Task(const QString& name, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
    connect(ui->descripbutton, &QPushButton::clicked, this, &Task::addDescrip);
    connect(ui->deadlinebutton, &QPushButton::clicked, this, &Task::setDeadline);
    connect(ui->assignbutton, &QPushButton::clicked, this, &Task::setAssign);
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name)
{
    ui->checkbox->setText(name);
}

void Task::setDate(const QString& deadline)
{
    ui->deadlined->setText("Deadline: "+deadline);
}

void Task::assign(const QString& assignee)
{
    ui->assignee->setText("Assigned to: "+assignee);
}

void Task::describe(const QString& description)
{
    ui->descripd->setText("Description: "+description);
}

QString Task::name() const
{
    return ui->checkbox->text();
}

bool Task::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::setDeadline()
{
    bool ok1;
    QString value = QInputDialog::getText(this, tr("Add a date"),
                                          tr("Date (format: ddMMyy)"), QLineEdit::Normal,
                                          this->name(), &ok1);
    if (ok1 && !value.isEmpty()) {
        QDate given_date=QDate::fromString(value,"ddMMyy");       // check format
        QString returned_date=given_date.toString("dd/MM/yy");
        setDate(returned_date);
    }

};

void Task::setAssign(){
    bool ok2;
    QString value = QInputDialog::getText(this, tr("Assign"),
                                          tr("Assign a referee"), QLineEdit::Normal,
                                          this->name(), &ok2);
    if (ok2 && !value.isEmpty()) {
        assign(value);
    }

};

void Task::addDescrip(){
    bool ok3;
    QString value = QInputDialog::getText(this, tr("Description"),
                                          tr("Add a description"), QLineEdit::Normal,
                                          this->name(), &ok3);
    if (ok3 && !value.isEmpty()) {
        describe(value);
    }
};

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
