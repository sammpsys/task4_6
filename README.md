# Task 4.6
The repo contains changes made to the original provided for this task. The changes include setting a deadline, assigning a task and adding a description.

## Requirements

Requires `QTCreator`.

## Add your files


Clone the repo and open the project in QTCreator. Build and run the project in QTCreator.

## Maintainers

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

## Contributors and License

Copyright 2022,

[Sam Nassiri @sammpsys](https://gitlab.com/sammpsys)

